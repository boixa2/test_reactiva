Este código pertenece a un proyecto antiguo que estamos refactorizando ya que tiene partes como las siguientes muy difíciles de mantener. Este caso de uso es el de crear un Ticket, a partir de este fragmento de código, plantear que problemas hay, cómo los podemos resolver, añadir todos los métodos/clases que sean pertinentes. Puede que haya errores en el código que se muestra. 
No hace falta que se implemente.

### Se pide:
Refactorizar el código presentado para que sea más mantenible, una solución más estructurada, organizada, con buenas prácticas, código limpio y entendible. 

No hace falta que se implementen las funciones o clases que se creen (si se crean), con las cabeceras y un comentario de lo que debería hacer (si es que se requiere para entenderlo mejor) nos vale.

Es interesante acompañar con comentarios aquellas partes donde se quiera hacer un inciso de lo que se pretende hacer.
Hay carta blanca para modificar como se quiera el código propuesto (eliminar clases, crear de nuevas, etc).

Que se destaca como positivo, que se destaca como negativo y como lo podríamos solucionar.

### Información para entender mejor el problema:
Este es el caso de uso para crear un ticket para una excursión en barco.
El usuario, que en este caso es un comercial de la empresa, selecciona que tipo de ticket quiere crear:
- Ticket Particular
El usuario escoge una salida (Sortida), número de asistentes desglosados en (adults, nens, nadons, especial adultos y especial niños) y información opcional acerca del cliente (nombre, apellido, etc) y puede añadir una serie de descuentos. Las plazas de este ticket son la suma de todos los tipos de asistentes menos los bebés, que no ocupan una plaza (silla) estos son adults, nens, especial adultos y especial niños.
Cada salida tiene un precio estipulado por tipo de asistente, precio adulto, precio niño precio bebe, los especiales se especifican cuando se crea el ticket. Se marca por ejemplo, 2 especial niños de precio 10€ cada uno. (Es un precio abierto).

- Ticket Agencia
El usuario escoge una salida, una agencia de la lista, el número de asistentes y un comentario con información de la agencia y la salida. Las plazas de este ticket son el número de asistentes. 
El precio del ticket agencia es siempre 0.

Una salida se hace con un barco, este barco tiene una capacidad de plazas máxima y un overbooking de plazas, nunca se puede acceder el número de plazas del barco + su overbooking.

Las salidas pertenecen a una ruta y pueden ser de empresas distintas, y la numeración del ticket tiene que ser correlativa por empresa, puede haber dos tickets con el mismo número, siempre que sean empresas distintas.

Si los tickets son una reserva se marcan con el estado pendiente, si son una venda o una invitación se marcan como pagados. La invitación tiene precio 0 euros. También se puede especificar un precio manual que se sobrepone al precio calculado por asistentes.

No se pueden hacer tickets sin ningún asistente.
