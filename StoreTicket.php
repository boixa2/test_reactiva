<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTicket extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (request()->tipo == 'agencia') {
            return [
                'agencia_id' => 'required',
                'sortida_id' => 'required',
                'places' => 'required',
                'passatgers' => 'required',
            ];
        }

        return [
            'sortida_id' => 'required',
            'adults' => 'numeric',
            'nens' => 'numeric',
            'nadons' => 'numeric',
            'altres_places' => 'numeric',
            'altres_places_menors' => 'numeric',
            'altres_preu' => 'required_if:altres_places,>=,1',
            'altres_preu_menors' => 'required_if:altres_places_menors,>=,1',
            'nacionalitat' => 'required',
            'origen' => 'required',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'sortida_id.required' => 'La sortida és obligatoria.',
            'altres_preu.required_if' => 'No has introduit el preu de les places.',
            'altres_preu_menors.required_if' => 'No has introduit el preu de les places.',
        ];
    }
}
