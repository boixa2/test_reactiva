<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTicket;
use Carbon\Carbon;
use App\Agencia;
use App\Ticket;
use App\Empresa;
use App\Ruta;
use App\Sortida;
use App\TicketAgencia;
use App\TicketParticular;

class TicketController extends Controller
{

    /**
     * Crea un nou ticket.
     *
     * @param StoreTicket $request
     * @param \TicketService $ticketService
     * @return mixed
     */
    public function store(StoreTicket $request, \TicketService $ticketService)
    {
        $sortida    = Sortida::find($request->sortida_id);
        $ruta       = Ruta::find($sortida->ruta->id);
        $empresa    = Empresa::find($ruta->empresa->id);
        $places     = $request->tipo == 'agencia' ? $request->places : $request->adults + $request->nens + $request->nadons;

        // TODO: Check + send errors if 'sortida', 'ruta' or 'empresa' not exist

        $sortidaService = new \SortidaService($sortida);
        if($sortidaService->sortidaPlena($places)) {
            return \ErrorService::sortidaPlenaError();
        }

        // TODO: Create Ticket object using factory pattern adding needed parameters
        $ticket = new Ticket([
            'codi' => $request->codi ? $request->codi : Ticket::ultimCodiEmpresa($empresa->id),
            'user_id' => auth()->user()->id,
            'punt_de_venda_id' => auth()->user()->punt_de_venda_id,
            'empresa_id' => $empresa->id,
            'sortida_id' => $request->sortida_id,
        ]);

        if ($request->tipo == 'agencia') {
            $ticketService->createTicket($ticket);
            //** Not recommended put all request data - developer didn't know what kind data it uses
            return $ticketService->createAndSaveTicketAgencia($request);
        }

        if($ticketService->totalAtLeastOnePerson(
            [$request->adults, $request->nens, $request->nadons, $request->altres_places, $request->altres_places_menors]
        )) {
            return ErrorService::minimUnaPersonaError();
        }

        /**
         * MOVE TO storeParticularTicket
         **/

        $ticketService->createDiscount($request->descomptes);

        // ** AJAX petitions run from another routes/controllers
        if ($request->ajax()) {
            return $ticketService->createdTicketCorrectlyAjaxMessage();
        }

        return redirect()->route('tickets.index');
    }

    /**
     * Crea un nou ticket particular.
     *
     * @param StoreTicket $request
     * @param \TicketService $ticketService
     * @return mixed
     */
    public function storeParticularTicket(StoreTicket $request, \TicketService $ticketService)
    {
        /*
        // ** Never add/remove request data - they comes from forms; if we need other data, we must to use other data and controllers
        if (empty($request->preu)) {
            $request->merge(['preu' => null]);
        }

        if($request->altres_places <= 0){
            unset($request['altres_preu']);
        }
        if($request->altres_places_menors <= 0){
            unset($request['altres_preu_menors']);
        }
        */
        $sortida            = Sortida::find($request->sortida_id);
        $ruta               = Ruta::find($sortida->ruta->id);
        $empresa            = Empresa::find($ruta->empresa->id);
        //** Not recommended put all request data - developer didn't know what kind data it uses
        $ticketParticular   = TicketParticular::create($request->all());

        // TODO: Create Ticket object using factory pattern adding needed parameters
        // ** In this case only have to instance TicketParticular class
        $ticket = new Ticket([
            'codi' => $request->codi ? $request->codi : Ticket::ultimCodiEmpresa($empresa->id),
            'user_id' => auth()->user()->id,
            'punt_de_venda_id' => auth()->user()->punt_de_venda_id,
            'empresa_id' => $empresa->id,
            'sortida_id' => $request->sortida_id,
        ]);

        $ticketParticular->ticket()->save($ticket);

        if ($ticketParticular->tipo != 1) {
            $ticket->estat = 1;
            $ticket->preu_total = $ticket->total();
            $ticket->save();
        }

        $ticketService->createDiscount($request->descomptes);

        if ($request->ajax()) {
            return $ticketService->createdTicketCorrectlyAjaxMessage();
        }

        return redirect()->route('tickets.index');
    }
}
