<?php
/**
 * Created by PhpStorm.
 * User: tony
 * Date: 14/10/18
 * Time: 13:42
 */

class ErrorService
{
    // Add error missages into constant or DB values

    public static function sortidaPlenaError()
    {
        return response()->json(['msg' => [
            "errors" => true,
            "title" => 'Error!',
            "text" => 'Aquesta sortida ja està plena!',
            "type" => 'error',
        ]], 200);
    }

    public static function minimUnaPersonaError()
    {
        return response()->json(['msg' => [
            "errors"    => true,
            "title"     => 'Error!',
            "text"      => 'Una tiquet ha de tindre com a mínim una persona',
            "type"      => 'error',
        ]], 200);
    }
}