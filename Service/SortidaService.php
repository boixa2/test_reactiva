<?php
/**
 * Created by PhpStorm.
 * User: tony
 * Date: 14/10/18
 * Time: 13:33
 */

class SortidaService
{
    /**
     * @var Sortida $sortida
     */
    private $sortida;

    /**
     * SortidaService constructor.
     * @param Sortida $sortida
     */
    public function __construct(Sortida $sortida)
    {
        $this->sortida = $sortida;
    }

    public function sortidaPlena($places)
    {
        return $places > ($this->sortida->ocupacio()->lliures + $this->sortida->overbooking);
    }

}