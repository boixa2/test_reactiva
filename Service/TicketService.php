<?php
/**
 * Created by PhpStorm.
 * User: tony
 * Date: 14/10/18
 * Time: 13:51
 */

class TicketService
{
    /**
     * @var Ticket $ticket
     */
    private $ticket;

    /**
     * @var Response $response
     */
    private $response;

    /**
     * TicketService constructor.
     * @param Response $response
     */
    public function __construct(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @param Ticket $ticket
     */
    public function createTicket(Ticket $ticket)
    {
        $this->ticket = $ticket;
    }

    /**
     * @param Request $request
     */
    public function createAndSaveTicketAgencia(Request $request)
    {
        $ticketAgencia          = new TicketAgencia($request->all());
        $ticketAgencia->preu    = Ticket::GRATUIT;
        $ticketAgencia->save();
        $ticketAgencia->ticket()->save($this->ticket);

        // Ticket entity has to implement get/set attributes
        if($request->reserva) {
            $this->ticket->estat = Ticket::PENDENT;
        } else {
            $this->ticket->estat = Ticket::PAGAT;
            $this->ticket->paid_at = Carbon::now();
        }

        $this->ticket->save();

        if ($request->ajax()) {
            return $this->response()->json(['msg' => [
                "title"     => 'Perfecte!',
                "text"      => "El ticket s'ha creat perfectament",
                "type"      => 'success',
                "id"        => $this->ticket->id,
                "agencia"   => true,
            ]], 200);
        }

        return redirect()->route('tickets.index');
    }

    /**
     * @param $array_people
     * @return bool
     */
    public function totalAtLeastOnePerson($array_people)
    {
        // Check there is at least 1 person
        if(array_sum($array_people) <= 0) {
            return false;
        }

        return true;
    }

    /**
     * @return JSON data
     */
    public function createdTicketCorrectlyAjaxMessage()
    {
        return $this->response()->json(['msg' => [
            "title"     => 'Perfecte!',
            "text"      => "El ticket s'ha creat perfectament",
            "type"      => 'success',
            "id"        => $this->ticket->id,
            "agencia"   => false,
        ]], 200);
    }

    /**
     * @param $discounts
     */
    public function createDiscount($discounts)
    {
        if (!empty($discounts)) {
            foreach ($discounts as $key => $descompte) {
                if ($descompte > 0) {
                    $this->ticket->descomptes()->attach([
                        $key => [
                            'quantitat' => $descompte
                        ]
                    ]);
                }
            }
        }

        return;
    }

}